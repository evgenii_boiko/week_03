package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArraySimpleListTest {

    public static final String FIRST_ELEMENT = "First element";
    public static final String SECOND_ELEMENT = "Second element";
    public static final String THIRD_ELEMENT = "Third element";
    public static final String TEST_ELEMENT = "Test element";
    public static final int CAPACITY = 3;
    private SimpleList<String> list;

    @Before
    public void setUp() throws Exception {
        list = new ArraySimpleList<>(CAPACITY);
    }

    @Test
    public void add() {
        assertEquals(0,list.size());

        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
        assertEquals(FIRST_ELEMENT, list.get(0));

        list.add(SECOND_ELEMENT);
        assertEquals(2, list.size());
        assertEquals(SECOND_ELEMENT, list.get(1));

        list.add(THIRD_ELEMENT);
        assertEquals(3, list.size());
        assertEquals(THIRD_ELEMENT, list.get(2));

        list.add(TEST_ELEMENT);
        assertEquals(4, list.size());
        assertEquals(TEST_ELEMENT, list.get(3));
    }

    @Test
    public void set() {
        assertEquals(0,list.size());

        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
        assertEquals(FIRST_ELEMENT, list.get(0));

        list.add(SECOND_ELEMENT);
        assertEquals(2, list.size());
        assertEquals(SECOND_ELEMENT, list.get(1));

        list.add(THIRD_ELEMENT);
        assertEquals(3, list.size());
        assertEquals(THIRD_ELEMENT, list.get(2));


        list.set(0, TEST_ELEMENT);
        assertEquals(3, list.size());
        assertEquals(TEST_ELEMENT, list.get(0));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void setNotPermitted() {
        list.set(4, TEST_ELEMENT);
        assertEquals(TEST_ELEMENT, list.get(5));
    }

    @Test
    public void get() {
        assertEquals(0,list.size());

        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
        assertEquals(FIRST_ELEMENT, list.get(0));

        list.add(SECOND_ELEMENT);
        assertEquals(2, list.size());
        assertEquals(SECOND_ELEMENT, list.get(1));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getNotPermitted() {
        assertEquals(0,list.size());

        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
        assertEquals(FIRST_ELEMENT, list.get(0));

        list.add(SECOND_ELEMENT);
        assertEquals(2, list.size());
        assertEquals(SECOND_ELEMENT, list.get(1));

        list.get(-1);
        list.get(5);
    }

    @Test
    public void remove() {
        assertEquals(0,list.size());

        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
        assertEquals(FIRST_ELEMENT, list.get(0));

        list.add(SECOND_ELEMENT);
        assertEquals(2, list.size());
        assertEquals(SECOND_ELEMENT, list.get(1));

        list.add(THIRD_ELEMENT);
        assertEquals(3, list.size());
        assertEquals(THIRD_ELEMENT, list.get(2));


        list.remove(0);
        assertEquals(2, list.size());
        assertEquals(SECOND_ELEMENT, list.get(0));

        list.add(TEST_ELEMENT);
        assertEquals(3, list.size());
        assertEquals(TEST_ELEMENT, list.get(2));
        list.remove(2);
        assertEquals(2, list.size());
        assertEquals(THIRD_ELEMENT, list.get(1));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeNotPermitted() {
        assertEquals(0,list.size());

        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
        assertEquals(FIRST_ELEMENT, list.get(0));

        list.remove(80);
    }

    @Test
    public void indexOf() {
        assertEquals(0,list.size());

        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
        assertEquals(FIRST_ELEMENT, list.get(0));
        assertEquals(0, list.indexOf(FIRST_ELEMENT));

        list.add(SECOND_ELEMENT);
        assertEquals(2, list.size());
        assertEquals(SECOND_ELEMENT, list.get(1));
        assertEquals(1, list.indexOf(SECOND_ELEMENT));

        assertEquals(-1, list.indexOf(TEST_ELEMENT));
    }

    @Test
    public void size() {
        assertEquals(0,list.size());

        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
    }
}