package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedSimpleListTest {

    public static final String FIRST_ELEMENT = "First element";
    public static final String SECOND_ELEMENT = "Second element";
    public static final String THIRD_ELEMENT = "Third element";
    public static final String FOURTH_ELEMENT = "Fourth element";
    public static final String FIFTH_ELEMENT = "Fifth element";
    public static final String TEST_ELEMENT = "Test element";
    private SimpleList<String> list;

    /**
     * перед каждым тестом внутри setUp инициализируем наш тестовый объект.
     * @throws Exception исключение, если список не объявлен
     */
    @Before
    public void setUp() throws Exception {
        list = new LinkedSimpleList<>();
    }

    @Test
    public void add() {
        assertEquals(0,list.size());

        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
        assertEquals(FIRST_ELEMENT, list.get(0));

        list.add(SECOND_ELEMENT);
        assertEquals(2, list.size());
        assertEquals(SECOND_ELEMENT, list.get(1));
    }

    @Test
    public void set() {
        assertEquals(0,list.size());

        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
        assertEquals(FIRST_ELEMENT, list.get(0));

        list.add(SECOND_ELEMENT);
        assertEquals(2, list.size());
        assertEquals(SECOND_ELEMENT, list.get(1));

        list.set(0, TEST_ELEMENT);
        assertEquals(TEST_ELEMENT, list.get(0));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void setNotPermitted() {
        list.set(5, TEST_ELEMENT);
        assertEquals(TEST_ELEMENT, list.get(5));
    }

    @Test
    public void get() {
        assertEquals(0,list.size());

        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
        assertEquals(FIRST_ELEMENT, list.get(0));

        list.add(SECOND_ELEMENT);
        assertEquals(2, list.size());
        assertEquals(SECOND_ELEMENT, list.get(1));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getNotPermitted() {
        assertEquals(0,list.size());

        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
        assertEquals(FIRST_ELEMENT, list.get(0));

        list.add(SECOND_ELEMENT);
        assertEquals(2, list.size());
        assertEquals(SECOND_ELEMENT, list.get(1));

        list.get(-1);
        list.get(5);
    }

    @Test
    public void remove() {
        assertEquals(0,list.size());

        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
        assertEquals(FIRST_ELEMENT, list.get(0));
        list.remove(0);
        assertEquals(0, list.size());


        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
        assertEquals(FIRST_ELEMENT, list.get(0));

        list.add(SECOND_ELEMENT);
        assertEquals(2, list.size());
        assertEquals(SECOND_ELEMENT, list.get(1));

        list.remove(0);
        assertEquals(1, list.size());
        list.remove(0);
        assertEquals(0, list.size());


        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
        assertEquals(FIRST_ELEMENT, list.get(0));

        list.add(SECOND_ELEMENT);
        assertEquals(2, list.size());
        assertEquals(SECOND_ELEMENT, list.get(1));

        list.remove(1);
        assertEquals(1, list.size());
        list.remove(0);
        assertEquals(0, list.size());


        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
        assertEquals(FIRST_ELEMENT, list.get(0));

        list.add(SECOND_ELEMENT);
        assertEquals(2, list.size());
        assertEquals(SECOND_ELEMENT, list.get(1));

        list.add(THIRD_ELEMENT);
        assertEquals(3, list.size());
        assertEquals(THIRD_ELEMENT, list.get(2));

        list.add(FOURTH_ELEMENT);
        assertEquals(4, list.size());
        assertEquals(FOURTH_ELEMENT, list.get(3));

        list.add(FIFTH_ELEMENT);
        assertEquals(5, list.size());
        assertEquals(FIFTH_ELEMENT, list.get(4));

        list.remove(3);
        assertEquals(FIFTH_ELEMENT, list.get(3));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeNotPermitted() {
        assertEquals(0,list.size());

        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
        assertEquals(FIRST_ELEMENT, list.get(0));

        list.remove(80);
    }

    @Test
    public void indexOf() {
        assertEquals(0,list.size());

        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
        assertEquals(FIRST_ELEMENT, list.get(0));
        assertEquals(0, list.indexOf(FIRST_ELEMENT));

        list.add(SECOND_ELEMENT);
        assertEquals(2, list.size());
        assertEquals(SECOND_ELEMENT, list.get(1));
        assertEquals(1, list.indexOf(SECOND_ELEMENT));

        assertEquals(-1, list.indexOf(TEST_ELEMENT));
    }

    @Test
    public void size() {
        assertEquals(0,list.size());

        list.add(FIRST_ELEMENT);
        assertEquals(1, list.size());
    }
}