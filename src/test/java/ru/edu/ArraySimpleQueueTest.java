package ru.edu;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArraySimpleQueueTest {
    public static final String FIRST_ELEMENT = "First element";
    public static final String SECOND_ELEMENT = "Second element";
    public static final String THIRD_ELEMENT = "Third element";
    public static final String FOURTH_ELEMENT = "Fourth element";
    public static final String FIFTH_ELEMENT = "Fifth element";
    public static final String TEST_ELEMENT = "Test element";
    public static final int CAPACITY = 3;
    private SimpleQueue<String> list;

    @Before
    public void setUp() throws Exception {
        list = new ArraySimpleQueue<>(CAPACITY);
    }

    @Test
    public void offer() {
        assertEquals(0,list.size());

        assertTrue(list.offer(FIRST_ELEMENT));
        assertEquals(1, list.size());

        assertTrue(list.offer(SECOND_ELEMENT));
        assertEquals(2, list.size());

        assertTrue(list.offer(THIRD_ELEMENT));
        assertEquals(3, list.size());


        assertFalse(list.offer(TEST_ELEMENT));
        assertEquals(4, list.size());

        assertTrue(list.offer(FIFTH_ELEMENT));
        assertEquals(5, list.size());
    }

    @Test
    public void poll() {
        assertEquals(0, list.size());
        assertNull(list.poll());
        assertEquals(0, list.size());

        assertTrue(list.offer(TEST_ELEMENT));
        assertEquals(1, list.size());
        assertEquals(TEST_ELEMENT, list.poll());
        assertEquals(0, list.size());

        assertTrue(list.offer(FIRST_ELEMENT));
        assertTrue(list.offer(SECOND_ELEMENT));
        assertTrue(list.offer(THIRD_ELEMENT));
        assertEquals(FIRST_ELEMENT, list.poll());
        assertEquals(2, list.size());
    }

    @Test
    public void peek() {
        assertEquals(0, list.size());
        assertNull(list.peek());
        assertEquals(0, list.size());

        assertTrue(list.offer(SECOND_ELEMENT));
        assertEquals(SECOND_ELEMENT, list.peek());
        assertEquals(1, list.size());
    }

    @Test
    public void size() {
        assertEquals(0,list.size());

        assertTrue(list.offer(FIRST_ELEMENT));
        assertEquals(1, list.size());

        assertTrue(list.offer(SECOND_ELEMENT));
        assertEquals(2, list.size());
    }

    @Test
    public void capacity() {
        assertEquals(CAPACITY, list.capacity());
    }
}