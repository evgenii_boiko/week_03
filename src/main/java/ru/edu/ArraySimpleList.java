package ru.edu;

public class ArraySimpleList<T> implements SimpleList<T> {

    /**
     * Объявление массива.
     */
    private T[] array;

    /**
     * Фактический (текущий) размер массива.
     */
    private int size;

    /**
     * Конструктор - объявление и инициализация массива для будущей коллекции.
     *  @param capacity вместимость
     */
    @SuppressWarnings("unchecked")
    public ArraySimpleList(final int capacity) {
        this.array = (T[]) new Object[capacity];
    }

    /**
     * Добавление элемента в конец списка.
     *
     *
     * @param value элемент
     */
    @Override
    @SuppressWarnings("unchecked")
    public void add(final T value) {
        if (size == array.length) {
            T[] oldArray = array;
            this.array = (T[]) new Object[array.length * 2];
            for (int i = 0; i < oldArray.length; i++) {
                array[i] = oldArray[i];
            }
        }
        array[size++] = value;
    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("SET index is out of the size");
        }
        array[index] = value;
    }

    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("GET index is out of the size");
        }
        return array[index];
    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * Если индекс меньше последнего индекса в массиве (index < size - 2),
     *
     * то мы сдвигаем элементы влево и обнуляем бывший последний элемент,
     *
     * уменьшая при этом размер массива на 1. Если же
     *
     * индекс равен последнему элементу, мы, не сдвигая, сразу обнуляем этот
     *
     * элемент (условие else),
     *
     * уменьшая при этом размер массива на 1.
     *
     * @param index индекс
     */
    @Override
    public void remove(final int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("REM index is out of the size");
        }
        if (index < size - 2) {
            for (int i = index + 1; i < size; i++) {
                array[i - 1] = array[i];
            }
            array[--size] = null;
        } else {
            array[--size] = null;
        }
    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return индекс элемента или -1 если не найден
     */
    @Override
    public int indexOf(final T value) {
        int index = -1;
        for (int i = 0; i < size; i++) {
            if (array[i].equals(value)) {
                index = i;
            }
        }
        return index;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return size;
    }
}
