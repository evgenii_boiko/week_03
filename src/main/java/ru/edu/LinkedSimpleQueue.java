package ru.edu;

public class LinkedSimpleQueue<T> implements SimpleQueue<T> {

    /**
     * head - текущее начало списка.
     */
    private Node<T> head;

    /**
     * tail - текущий конец списка.
     */
    private Node<T> tail;

    /**
     * Вместимость - максимальный размер списка.
     */
    private final int capacity;

    /**
     * Фактический (текущий) размер массива.
     */
    private int size;

    /**
     * Конструктор очереди с заданной фиксированной емкостью.
     * @param cap вместимость
     */
    public LinkedSimpleQueue(final int cap) {
        this.capacity = cap;
    }

    /**
     * Инициализируем приватный статический класс.
     * Внутри класса объявляем конструктор для элемента (ноды)
     * @param <T>
     */
    private static class Node<T> {

        /**
         * previous - предыдущее значение, на которое указывает node.
         */
        private Node<T> previous;

        /**
         * next - следующее значение, на которое указывает node.
         */
        private Node<T> next;

        /**
         * value - текущее значение node, с которым работаем.
         */
        private T value;

        Node(final T val) {
            this.value = val;
        }
    }

    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть место в очереди)
     */
    @Override
    public boolean offer(final T value) {
        Node<T> node = new Node<>(value);

        if (head == null) {
            head = node;
            tail = node;
        } else if (size != capacity) {
            node.previous = tail;
            tail.next = node;
            tail = node;
        } else {
            return false;
        }
        size++;
        return true;
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T poll() {
        T returnElement;
        if (size == 0) {
            head = null;
            tail = null;
            return null;
        } else {
            returnElement = head.value;
            Node<T> next = head.next;
            if (next != null) {
                next.previous = null;
            }
            head = next;
            size--;
        }
        return returnElement;
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {
        T returnElement;
        if (size == 0) {
            return null;
        } else {
            returnElement = head.value;
        }
        return returnElement;
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return this.capacity;
    }
}
