package ru.edu;

public class LinkedSimpleList<T> implements SimpleList<T> {

    /**
     * head - текущее начало списка.
     */
    private Node<T> head;

    /**
     * tail - текущий конец списка.
     */
    private Node<T> tail;

    /**
     * Фактический (текущий) размер массива.
     */
    private int size;


    /**
     * Инициализируем приватный статический класс.
     * Внутри класса объявляем конструктор для элемента (ноды)
     * @param <T>
     */

    private static class Node<T> {

        /**
         * previous - предыдущее значение, на которое указывает node.
         */
        private Node<T> previous;

        /**
         * next - следующее значение, на которое указывает node.
         */
        private Node<T> next;

        /**
         * value - текущее значение node, с которым работаем.
         */
        private T value;

        Node(final T val) {
            this.value = val;
        }
    }


    /**
     * Добавление элемента в конец списка.
     *
     * @param value элемент
     */
    @Override
    public void add(final T value) {
        Node<T> node = new Node<>(value);

        if (head == null) {
            head = node;
        } else {
            node.previous = tail;
            tail.next = node;
        }
        tail = node;
        size++;
    }

    /**
     * Установка значения элемента по индексу.
     *
     * @param index индекс
     * @param value элемент
     */
    @Override
    public void set(final int index, final T value) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("SET index is out of the size");
        }
        findNode(index).value = value;
    }
    /**
     * Получение элемента из списка.
     *
     * @param index индекс
     * @return значение элемента или null
     */
    @Override
    public T get(final int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("GET index is out of the size");
        }
        Node<T> node = findNode(index);
        return node.value;
    }

    private Node<T> findNode(final int index) {
        Node<T> node;

        if (index <= size / 2) {
            node = head;

            for (int i = 0; i < index; i++) {
                node = node.next;
            }
        } else {
            node = tail;

            for (int i = 0; i < size - (index + 1); i++) {
                node = node.previous;
            }
        }
        return node;
    }

    /**
     * Удаление элемента по индексу.
     * При удалении происходит сдвиг элементов влево, начиная с index+1 и далее.
     *
     * @param index индекс
     */
    @Override
    public void remove(final int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("REM index is out of the size");
        }
        removeElement(index);
        size--;
    }

    private void removeElement(final int index) {
        if (size == 1) {
            head = null;
            tail = null;
            return;
        }
        if (index == 0) {
            Node<T> next = head.next;
            if (next != null) {
                next.previous = null;
            }
            head = next;
            return;
        }
        if (index == size - 1) {
            Node<T> previous = tail.previous;
            previous.next = null;
            tail = previous;
        } else {
            Node<T> remove = findNode(index);

            remove.previous.next = remove.next;
            remove.next.previous = remove.previous;
        }
    }

    /**
     * Получение индекса элемента по его значению.
     *
     * @param value элемент
     * @return индекс элемента или -1 если не найден
     */
    @Override
    public int indexOf(final T value) {

        Node<T> node = head;
        for (int i = 0; i < size; i++) {
            if (node.value.equals(value)) {
                return i;
            }
            node = node.next;
        }
        return -1;
    }

    /**
     * Получение размера списка(количество элементов).
     *
     * @return размер списка
     */
    @Override
    public int size() {
        return size;
    }
}
