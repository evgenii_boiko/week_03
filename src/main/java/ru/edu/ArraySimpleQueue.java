package ru.edu;

public class ArraySimpleQueue<T> implements SimpleQueue<T> {

    /**
     * Объявление массива.
     */
    private T[] array;

    /**
     * Фактический (текущий) размер массива.
     */
    private int size;

    /**
     * Конструктор массива.
     *
     *  h
     * [               ]
     *  t
     *  После добавления элемента:
     *  h
     * [a               ]
     *    t
     * @param capacity вместимость
     */
    @SuppressWarnings("unchecked")
    public ArraySimpleQueue(final int capacity) {
        this.array = (T[]) new Object[capacity];
    }


    /**
     * Добавление элемента в конец очереди.
     *
     * @param value элемент
     * @return true - если удалось поместить элемент (если есть
     * место в очереди).
     *
     * Если места в очереди нет - false
     */
    @Override
    @SuppressWarnings("unchecked")
    public boolean offer(final T value) {
        boolean offerIndicator = true;
        if (size == array.length) {
            offerIndicator = false;
            T[] oldArray = array;
            this.array = (T[]) new Object[array.length * 2];
            for (int i = 0; i < oldArray.length; i++) {
                array[i] = oldArray[i];
            }
        }
        array[size++] = value;
        return offerIndicator;
    }

    /**
     * Получение и удаление первого элемента из очереди.
     *
     * @return первый элемент из очереди. Если же очередь пуста
     * (ни одного элемента нет) - return null
     */
    @Override
    public T poll() {
        T returnElement;
        if (array[0] == null) {
            returnElement = null;
        } else if (array[1] == null) {
            returnElement = array[0];
            array[0] = null;
            --size;
        } else {
            returnElement = array[0];
            for (int i = 1; i < size; i++) {
                array[i - 1] = array[i];
            }
            array[--size] = null;
        }
        return returnElement;
    }

    /**
     * Получение БЕЗ удаления первого элемента из очереди.
     *
     * @return первый элемент из очереди
     */
    @Override
    public T peek() {
        return array[0];
    }

    /**
     * Количество элементов в очереди.
     *
     * @return количество элементов
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Количество элементов которое может уместиться в очереди.
     *
     * @return -1 если не ограничено (будет расширяться),
     * либо конкретное число если ограничено.
     */
    @Override
    public int capacity() {
        return array.length;
    }
}
